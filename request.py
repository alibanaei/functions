import requests

headers = {
    'authorization': 'Basic MjNiYzQ2YjEtNzFmNi00ZWQ1LThjNTQtODE2YWE0ZjhjNTAyOjEyM3pPM3haQ0xyTU42djJCS0sxZFhZRnBYbFBrY2NPRnFtMTJDZEFzTWdSVTRWck5aOWx5R1ZDR3VNREdJd1A=',
    'content-type': 'application/json',
}

params = (
    ('blocking', 'true'),
    ('result', 'true'),
)

data = '{"number" : 40}'

response = requests.post('https://10.10.0.1/api/v1/namespaces/guest/actions/fib', headers=headers, params=params, data=data, verify=False)
print(response.json())
