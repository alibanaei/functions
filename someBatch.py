from threading import Thread
from time import sleep
import requests
import time


def request(id, memory):
    start = time.time()
    headers = {
        'authorization': 'Basic MjNiYzQ2YjEtNzFmNi00ZWQ1LThjNTQtODE2YWE0ZjhjNTAyOjEyM3pPM3haQ0xyTU42djJCS0sxZFhZRnBYbFBrY2NPRnFtMTJDZEFzTWdSVTRWck5aOWx5R1ZDR3VNREdJd1A=',
        'content-type': 'application/json',
    }

    params = (
        ('blocking', 'true'),
        ('result', 'true'),
    )

    data = '{"number" : 30}'

    response = requests.post('https://10.10.0.1/api/v1/namespaces/guest/actions/fib' + `memory`, headers=headers,
                             params=params,
                             data=data, verify=False)
    end = time.time()
    total = end - start
    print(id, total , response)
    sleep(1)


threads = []
for i in range(20):
        t = Thread(target=request, args=[i, 128])
        threads.append(t)
        t.start()

for t in threads:
    t.join()
